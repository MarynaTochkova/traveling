﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Travelling.Infrastructure
{
    public interface ICacheProviderService
    {
        Task Add(string key, string value);

        Task <string> Get(string key);

        IEnumerable<string> GetAll();

        void Delete(string key);
    }
}
