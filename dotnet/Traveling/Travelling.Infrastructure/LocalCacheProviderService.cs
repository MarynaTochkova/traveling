﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace Travelling.Infrastructure
{
    public class LocalCacheProviderService : ICacheProviderService
    {

        private readonly MemoryCache _cache = MemoryCache.Default;
        public async Task Add(string key, string value)
        {
            await
                Task.Run(
                    () =>
                        _cache.Add(new CacheItem(key, value),
                            new CacheItemPolicy {AbsoluteExpiration = DateTime.UtcNow.AddDays(10)}));

        }

        public async Task<string> Get(string key)
        {
            return await Task.Run(() => _cache.Get(key).ToString());
        }

        public IEnumerable<string> GetAll()
        {
            // if Azure is not used then there is no WebJob exists,
            return Enumerable.Empty<string>();
        }

        public void Delete(string key)
        {
            _cache.Remove(key);
        }
    }
}
