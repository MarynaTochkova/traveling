﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using StackExchange.Redis;
using StackExchange.Redis.Extensions.Core;
using StackExchange.Redis.Extensions.Newtonsoft;

namespace Travelling.Infrastructure
{
    public class RedisCacheService : ICacheProviderService
    {
        private static readonly Lazy<ConnectionMultiplexer> _connection =
            new Lazy<ConnectionMultiplexer>(() => ConnectionMultiplexer.Connect(
                "donottouchredis.redis.cache.windows.net:6380,password=x7Y1mC8XSdfkyLlA8QEFNPRfWQK9InEvL0rFAOYUugc=,ssl=True,abortConnect=False"));

        private static IDatabase Cache
        {
            get
            {
                return _connection.Value.GetDatabase();
            }
        }

        public async Task Add(string key, string value)
        {
            await Cache.StringSetAsync(key, value, TimeSpan.FromDays(10));
        }

        public async Task<string> Get(string key)
        {
            return await Cache.StringGetAsync(key);
        }

        public IEnumerable<string> GetAll()
        {
            //used only by webjob
            var serializer = new NewtonsoftSerializer();
            var cacheClient = new StackExchangeRedisCacheClient(serializer,_connection.Value.Configuration);
            return cacheClient.SearchKeys("*");
        }

        public void Delete(string key)
        {
            Cache.KeyDelete(key);
        }
    }
}