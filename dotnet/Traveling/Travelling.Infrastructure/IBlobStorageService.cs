﻿using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Travelling.Infrastructure
{
    public interface IBlobStorageService
    {
        Task AddBlob(string fileName, string filePath);

        Task DeleteBlobAsync(string name);
        bool BlobExists(string name);
        //Todo - get image

        string  GetFilePath(string name);
        string GetMimeType(string name);
        Task UpdateBlob(string newFilePath, string oldFilePath);
    }
}
