﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Travelling.Infrastructure
{
    public class AzureBlobStorageService //: IBlobStorageService
    {
        private const string _accountName = "storagetestdonottouch";
        private const string _accountKey = "hQ2m1aMsABh05dYgmZ5nc9RuIbHHcomUA4miDmbIxn8sCWgSSsha1HhhmvvABh5tPu7w4NKyPLI4dRmlEMOUjw==";
        private const string _containerName = "samples";


        public async Task<string> AddBlob(CloudBlobContainer container, string fileName, string filePath)
        {
            var blob = container.GetBlockBlobReference(fileName);

            using (Stream file = File.OpenRead(filePath))
            {
                await blob.UploadFromStreamAsync(file);
            }

            return blob.Uri.ToString();
        }

        public CloudBlobContainer GetContainer()
        {
            var creds = new StorageCredentials(_accountName, _accountKey);
            var account = new CloudStorageAccount(creds, useHttps: true);

            CloudBlobClient client = account.CreateCloudBlobClient();

            CloudBlobContainer container = client.GetContainerReference(_containerName);


            container.CreateIfNotExists();
            container.SetPermissions(
                    new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

            return container;
        }

        public async Task DeleteBlobAsync(CloudBlobContainer container, string name)
        {
            var blob = container.GetBlockBlobReference(name);
            try
            {
                await blob.DeleteAsync();
            }
            catch (StorageException exp)
            {
                try
                {
                    blob.DeleteAsync().Wait();
                }
                catch (Exception inner)
                {
                    // log
                }
            }
        }


        public bool BlobExists(CloudBlobContainer container, string name)
        {
            var blob = container.GetBlockBlobReference(name);
            return blob != null;
        }
    }
}