﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;

namespace Travelling.Infrastructure
{
    public class LocalStorageProvider : IBlobStorageService
    {

        public async Task AddBlob(string fileName, string filePath)
        {
            var newLocation = Path.Combine(StorageLocation, fileName);

            await Task.Run(() => File.Copy(filePath, newLocation, true));
        }

        public async Task DeleteBlobAsync(string name)
        {
            var location = Path.Combine(StorageLocation, name);
            try
            {
                await Task.Run(() => File.Delete(location));
            }
            catch (FileNotFoundException exp)
            {
                // log
            }
        }

        public string StorageLocation
        {
            get
            {
                var baseLocation = AppDomain.CurrentDomain.BaseDirectory;
                return Path.Combine(baseLocation, "App_Data", "Storage");
            }
        }


        public bool BlobExists(string name)
        {
            var location = Path.Combine(StorageLocation, name);
            return File.Exists(location);
        }

        public string GetFilePath(string name)
        {
            return Path.Combine(StorageLocation, name);
        }

        public string GetMimeType(string fileName)
        {
            var extention = fileName.Split('.').Last();
            switch (extention)
            {
                case "png":
                    return "image/png";
                case "jpg":
                case "jpeg":
                case "jpe":
                    return "image/jpeg";
                default:
                    return "hz";
            }
        }

        public async Task UpdateBlob(string newFilePath, string oldFilePath)
        {
            await Task.Run(() => File.Copy(oldFilePath, newFilePath, true));
        }
    }
}
