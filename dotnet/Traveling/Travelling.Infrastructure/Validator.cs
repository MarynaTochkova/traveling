﻿using System;
using System.Linq.Expressions;

namespace Travelling.Infrastructure
{
    public static class Validator
    {
        public static void IsNotNull<T>(T param, Expression<Func<T>> parameterExpression) where T : class
        {
            if (param == null)
            {
                throw new ArgumentNullException(GetMemberName(parameterExpression));
            }
        }

        public static void IsNotEmpty(Guid param, Expression<Func<Guid>> parameterExpression)
        {
            if (param == Guid.Empty)
            {
                throw new ArgumentNullException(GetMemberName(parameterExpression));
            }
        }

        private static string GetMemberName(LambdaExpression expression)
        {
            var body = (MemberExpression) expression.Body;
            return body.Member.Name;
        }
    }
}