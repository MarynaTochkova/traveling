﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Travelling.Entity;
using SQLite.CodeFirst;

namespace Travelling.Infrastructure
{
    public class TravellingDb : DbContext, ITravellingDb
    {
        public TravellingDb()
            : base("name=SQLiteConnection")
        {
            
        }
        public DbSet<Story> Stories { get; set; }

        public DbSet<Picture> Pictures { get; set; }

        public DbSet<City> Cities { get; set; }

        IQueryable<T> ITravellingDb.Query<T>()
        {
            return Set<T>();
        }

        T ITravellingDb.Add<T>(T entity)
        {
            return Set<T>().Add(entity);
        }

        void ITravellingDb.AddRange<T>(IEnumerable<T> entities)
        {
            Set<T>().AddRange(entities);
        }

        void ITravellingDb.RemoveRange<T>(IEnumerable<T> entities)
        {
            Set<T>().RemoveRange(entities);
        }

        void ITravellingDb.Update<T>(T entity)
        {
            Entry(entity).State = EntityState.Modified;
        }

        void ITravellingDb.Remove<T>(T entity)
        {
            Set<T>().Remove(entity);
        }

        Task<int> ITravellingDb.SaveChangesAsync()
        {
            return SaveChangesAsync();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var sqLiteConnectionInitializer = new SqliteCreateDatabaseIfNotExists<TravellingDb>(modelBuilder);
            Database.SetInitializer(sqLiteConnectionInitializer);
            //base.OnModelCreating(modelBuilder);
        }
    }
}