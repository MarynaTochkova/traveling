﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Travelling.Entity
{
    public class Story
    {
        [Key]
        [Index(IsUnique=true)]
        public Guid Id { get; set; }

        public string  Title { get; set; }

        public string Text { get; set; }

        public bool IsApproved { get; set; }

        [ForeignKey("City")]
        public int CityId { get; set; }

        public virtual City City { get; set; }

        public virtual ICollection<Picture> Pictures { get; set; }
        //add Status
    }
}