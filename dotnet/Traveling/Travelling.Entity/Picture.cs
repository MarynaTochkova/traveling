﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Travelling.Entity
{
    public class Picture
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public Guid StoryId { get; set; }

        public string OriginalName { get; set; }

        public string Name { get; set; }

        public DateTime UploadedTime { get; set; }
    }
}