namespace Travelling.Portal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Pictures",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StoryId = c.Guid(nullable: false),
                        OriginalName = c.String(),
                        Name = c.String(),
                        UploadedTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Stories",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Title = c.String(),
                        Text = c.String(),
                        IsApproved = c.Boolean(nullable: false),
                        CityId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cities", t => t.CityId, cascadeDelete: true)
                .Index(t => t.CityId);
            
            CreateTable(
                "dbo.Cities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Stories", "CityId", "dbo.Cities");
            DropIndex("dbo.Stories", new[] { "CityId" });
            DropTable("dbo.Cities");
            DropTable("dbo.Stories");
            DropTable("dbo.Pictures");
        }
    }
}
