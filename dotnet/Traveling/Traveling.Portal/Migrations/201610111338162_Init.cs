namespace Travelling.Portal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pictures", "PictureUrl", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Pictures", "PictureUrl");
        }
    }
}
