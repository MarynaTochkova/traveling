using Microsoft.Practices.Unity;
using System.Web.Http;
using Travelling.Infrastructure;
using Travelling.Portal.Infrastructure;
using Unity.WebApi;

namespace Travelling.Portal
{
    public static class UnityConfig
    {

        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            container.RegisterType<IBlobStorageService, LocalStorageProvider>();
            container.RegisterType<ICacheProviderService, LocalCacheProviderService>();
            container.RegisterType<ITravellingDb, TravellingDb>();
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}