﻿using System;
using System.Collections.Generic;

namespace Travelling.Portal.Models
{
    public class NewStoryViewModel
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Text { get; set; }

        public string City { get; set; }

        public IList<string> Pictures { get; set; }
    }
}