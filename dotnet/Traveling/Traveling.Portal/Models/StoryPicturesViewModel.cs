﻿using System;
using System.Collections.Generic;

namespace Travelling.Portal.Models
{
    public class StoryPicturesViewModel
    {
        public Guid StoryId { get; set; }

        public IList<UploadedPictureViewModel> Pictures { get; set; }
    }
}