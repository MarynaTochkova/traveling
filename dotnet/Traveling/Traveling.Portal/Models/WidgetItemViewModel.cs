﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Travelling.Portal.Models
{
    public class WidgetItemViewModel
    {
        public int CityId { get; set; }

        public string CityName { get; set; }

        public Guid StoryId { get; set; }

        public string StoryTitle { get; set; }

        public string StoryText { get; set; }

        public PictureViewModel ShownPicture { get; set; }
    }
}