﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Travelling.Portal.Models
{
    public class CityViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public IList<StoryModelView> Stories { get; set; }
    }
}