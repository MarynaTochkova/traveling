﻿
namespace Travelling.Portal.Models
{
    public class PictureViewModel
    {
        public int Id { get; set; }

        public string OriginalName { get; set; }

        public string Name { get; set; }

		public string PictureData { get; set; }
    }
}