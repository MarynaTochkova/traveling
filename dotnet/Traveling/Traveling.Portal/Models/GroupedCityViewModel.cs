﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;

namespace Travelling.Portal.Models
{
    public class GroupedCityViewModel
    {
        public string Name  { get; set; }

        public int CityId { get; set; }

        public IEnumerable<Guid> Stories { get; set; }
    }
}