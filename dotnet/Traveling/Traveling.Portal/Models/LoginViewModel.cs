﻿using System.Runtime.Serialization;

namespace Travelling.Portal.Models
{
    [DataContract]
    public class LoginViewModel
    {
        [DataMember(Name = "user")]
        public string UserName { get; set; }

        [DataMember(Name="password")]
        public string Password { get; set; }
    }
}