﻿
namespace Travelling.Portal.Models
{
    public class UploadedPictureViewModel
    {
        public string OriginalName { get; set; }

        public string Name { get; set; }

        public int Id { get; set; }
    }
}