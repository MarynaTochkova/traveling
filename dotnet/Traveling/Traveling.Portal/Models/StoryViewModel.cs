﻿using System;
using System.Collections.Generic;

namespace Travelling.Portal.Models
{
    public class StoryModelView
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Text { get; set; }

        public string City { get; set; }

        public ICollection<PictureViewModel> Pictures { get; set; } 
    }
}