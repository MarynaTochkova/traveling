/**
 * Created by Maryna on 7/29/2016.
 */

define(["jquery"], function () {

    return function (param) {
        var storyId = param;

        $(document).on("click", ".delete-photo", function () {

            event.preventDefault();
            //geting photo id
            var photoId = $(event.target).parent().find('img').attr('alt');
            var photoDiv = $(event.target).parent();

            var webSiteUrl = window.location.protocol + "//" + window.location.host + "/";
            if (location.pathname.split('/').length > 5) {
                webSiteUrl += location.pathname.split('/')[1];
            };
            var urlDeletePh = webSiteUrl + "api/picture/" + photoId + "/" + storyId;

            $.ajax({
                type: 'delete',
                url: urlDeletePh
            }).done(function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200 || jqXHR.status == 204) {
                    photoDiv.remove();
                    require(["modal-moderation"], function (modal) {
                        modal.photoDeleted();
                    });
                }
                else {
                    throw new Error(jqXHR.status);                 
                }
            });;
        });
    }
});