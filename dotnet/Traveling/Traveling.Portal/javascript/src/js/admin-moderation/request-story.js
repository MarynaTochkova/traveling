/**
 * Created by Maryna on 12/12/2016.
 */

define([], function () {

    $('div.list-group').on("click", function (event) {
        event.preventDefault();
        //clean the dom from previous photos
        $(".col-md-8").empty();

        if ($(event.target).hasClass('listStyle')) {
            var storyId = $(event.target).attr("href");
                nameTextarea = $('textarea')[0],
                titleTextarea = $('textarea')[1],
                textTextarea = $('textarea')[2];

            require(["delete-photo"], function (photoFunct) {
                photoFunct(storyId); 
            });

            require(["text-moderation-and-approved"], function (textModeration) {
                textModeration(storyId); 
            });

            require(["decline-story"], function (declineStory) {
                declineStory(storyId); 
            });           

            var weHaveSuccess = true;

            var webSiteUrl = window.location.protocol + "//" + window.location.host + "/";
            if (location.pathname.split('/').length > 5) {
                webSiteUrl += location.pathname.split('/')[1];
            }
            var callUrl = webSiteUrl + 'api/test/' + storyId;

            $.ajax({
                type: 'get',
                url: callUrl,
                contentType: "application/json; charset=utf-8",
                traditional: true,
                success: function (data, textStatus, xhr) {

                    for (var i in data) {
                        if (i == "City") {
                            $(nameTextarea).val(data[i]);
                        }
                        else if (i == "Title") {
                            $(titleTextarea).val(data[i]);
                        }
                        else if (i == "Text") {
                            $(textTextarea).val(data[i]);
                        }
                        else if (i == "Pictures") {
                            jQuery.each(data[i], function (ind, val) {
                                var url,
                                    id;
                                for (var key in val) {

                                    if (key == "Name") {
                                        url = webSiteUrl + "/api/picture?name=" + val[key];
                                    }
                                    else if (key == "Id") {
                                        id = val[key];
                                    }
                                }
                                if (url && id) {
                                    var image = new Image();
                                    image.src = val.PictureData;
                                 var newPhoto =
                                    `<a href=${image.src} data-toggle="lightbox" data-gallery="example-gallery" class="col-sm-4" data-max-width="600">
                                            <img src=${image.src} alt=${id} class="img-fluid">
                                            <button class="btn btn-danger delete-photo"> Delete </button>
                                         </a>`;
                                    $(".col-md-8").append(newPhoto);
                                }
                                else {
                                    weHaveSuccess = false;
                                    console.log(url + "  " + id + " no ");
                                }
                            });
                        }
                    }
                },

                error: function (xhr, textStatus, errorThrown) {
                    console.log("Error in getting story by id from server" + "  " + errorThrown.status);
                },
            });
        }
    });
});

