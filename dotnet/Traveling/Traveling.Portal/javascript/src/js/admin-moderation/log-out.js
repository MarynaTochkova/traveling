/**
 * Created by Maryna on 11/18/2016.
 */

define(['../common', 'bootstrap', 'domReady!'], function (){

    return $('#logOut').on("click", function () {
        $.ajax({
            type: 'post',
            url: '/api/logout',
            contentType: "application/json; charset=utf-8",
            traditional: true,
            success: function (data, textStatus, xhr) {
                window.location.assign("admin-authentication.html");
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log("Log out error" + " " + errorThrown.status);
            }
        });
    });
    
});