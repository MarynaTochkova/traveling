﻿
define([], function () {

    return function (param) {

        //preparing for ajax
        var webSiteUrl = window.location.protocol + "//" + window.location.host + "/";
        if (location.pathname.split('/').length > 5) {
            webSiteUrl += location.pathname.split('/')[1];
        };
        var storyId = param; 
        var urlTextModeration = webSiteUrl + "api/story/" + storyId; 
    
        $(".btn-danger").on("click", function () {
            $.ajax({
                type: "delete",
                url: urlTextModeration,
                contentType: "application/json; charset=utf-8",
                traditional: true,
                success: function (data, textStatus, xhr) {
                    require(["modal-moderation"], function (modal) {
                        modal.declineStory();
                    });
                },

                error: function (xhr, textStatus, errorThrown) {
                    console.log("Error by declining story" + "  " + errorThrown.status);
                },
            })
        });
    }
});