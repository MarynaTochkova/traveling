﻿define([], function () {

    return function (param) {

        var id = param; 

        $(".btn.btn-success.pull").on("click", function () {

            //preparing for ajax
            var webSiteUrl = window.location.protocol + "//" + window.location.host + "/";
            if (location.pathname.split('/').length > 5) {
                webSiteUrl += location.pathname.split('/')[1];
            };
            var urlTextModeration = webSiteUrl + "api/story/approve/" + id;

            var moderatedTextObj = {
                city: $($('textarea')[0]).val(),
                title: $($('textarea')[1]).val(),
                text: $($('textarea')[2]).val(),
                id: id
            };

            $.ajax({
                type: 'put',
                url: urlTextModeration,
                data: JSON.stringify(moderatedTextObj),
                contentType: "application/json; charset=utf-8",
                traditional: true,
                success: function (data, textStatus, xhr) {
                    require(["modal-moderation"], function (modal) {
                        modal.storyModeratedAndApproved();
                    });
                },
                error: function (xhr, textStatus, errorThrown) {
                    console.log(xhr.status + ': ' + xhr.statusText);
                }
            });
        });
    };
});