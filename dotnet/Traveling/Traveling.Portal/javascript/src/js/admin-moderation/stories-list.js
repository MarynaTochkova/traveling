/**
 * Created by Maryna on 11/22/2016.
 */

define(["jquery", 'domReady!'], function ($) {

    var webSiteUrl = window.location.protocol + "//" + window.location.host + "/";
    if (location.pathname.split('/').length > 5) {
        webSiteUrl += location.pathname.split('/')[1];
    }
    var callUrl = webSiteUrl + 'api/story/moderate';
           $.ajax({
               type: 'get',
               url: callUrl,
               contentType: "application/json; charset=utf-8",
               traditional: true,
               success: function (data, textStatus, xhr) {
                   $.each( data, function( key, value ) {
                       var txt2 = $("<a></a>").addClass("listStyle");
                       var elText;
                       var id;
                       for (var i in value) {
                           if (i == "City") {
                               elText = value[i];
                           }
                           else if (i == "Id") {
                               id = value[i];
                           }
                       }
                       txt2.text(elText);
                       txt2.attr("href", id);
                       $( ".list-group" ).append( txt2, $( "<br>" ) );                      
                   });
               },
               error: function (xhr, textStatus, errorThrown) {
                   console.log("Error in getting stories from server" + " " + errorThrown.status);
               }
           });    
});