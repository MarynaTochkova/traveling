﻿
define(["jquery"], function () {

    return function () {

        $(document).on("click", ".rotate-btn-left", function () {

            event.preventDefault();

            var selectedPhoto = $(event.target).parent().parent().find("img").addClass("element-to-rotate-left");
            var photoId = selectedPhoto.attr("alt");

            //require(["rotate"], function (rotate) {

                //rotate(selectedPhoto); 

                //selectedPhoto.rotateRight();
                var urlAndformDataObj = {};
                var rotatedPhoto = $(event.target).parent().parent().find("canvas");

                var ajaxPreparation = function (photo) {
                    //converting img to Blob

                    function base64ToBlob(base64, mime) {
                        mime = mime || '';
                        var sliceSize = 1024;
                        var byteChars = window.atob(base64);
                        var byteArrays = [];

                        for (var offset = 0, len = byteChars.length; offset < len; offset += sliceSize) {
                            var slice = byteChars.slice(offset, offset + sliceSize);

                            var byteNumbers = new Array(slice.length);
                            for (var i = 0; i < slice.length; i++) {
                                byteNumbers[i] = slice.charCodeAt(i);
                            }

                            var byteArray = new Uint8Array(byteNumbers);

                            byteArrays.push(byteArray);
                        }

                        return new Blob(byteArrays, { type: mime });
                    }

                    var formData = new FormData();
                    var image = rotatedPhoto[0].toDataURL("image/png");
                    var base64ImageContent = image.replace(/^data:image\/(png|jpeg|jpg);base64,/, "");
                    var blob = base64ToBlob(base64ImageContent, 'image/png');
                    formData.append('picture', blob);

                    var webSiteUrl = window.location.protocol + "//" + window.location.host + "/";
                    if (location.pathname.split('/').length > 5) {
                        webSiteUrl += location.pathname.split('/')[1];
                    }

                    urlAndformDataObj['url'] = webSiteUrl;
                    urlAndformDataObj['formData'] = formData;
                };

                ajaxPreparation(rotatedPhoto);

                $.ajax({
                    url: urlAndformDataObj.url + "/api/picture/" + photoId,
                    data: urlAndformDataObj.formData,
                    type: 'PUT',
                    processData: false,  // tell jQuery not to process the data
                    contentType: false,   // tell jQuery not to set contentType
                    method: 'PUT',
                    cache: false,
                    success: function (data, textStatus, xhr) {
                        console.log(urlAndformDataObj.formData);
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        console.log(xhr.status + ': ' + xhr.statusText);
                    }
                });  
            //});


        });
    }
});