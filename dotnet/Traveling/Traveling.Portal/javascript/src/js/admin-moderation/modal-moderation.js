﻿

define([], function () {
    var modalModeration = {
        photoDeleted: function () {
            var modalObj = $("#modalDeletedPhoto").modal();
            modalObj.modal('show');
            $("#closeModalDeletedPhoto").click(function () {
                modalObj.modal('hide');
            });
        },

        storyModeratedAndApproved: function () {
            var modalObj = $("#modalStoryModeratedAndApproved").modal();
            modalObj.modal('show');
            $("#closeModalStoryModeratedAndApproved").click(function () {
                modalObj.modal('hide');
                location.reload();
            });
        },

        declineStory: function () {
            var modalObj = $("#modalDeclineStory").modal();
            modalObj.modal('show');
            $("#closeModalDeclineStory").click(function () {
                modalObj.modal('hide');
                location.reload();
            });
        }

    };
    return modalModeration; 
});