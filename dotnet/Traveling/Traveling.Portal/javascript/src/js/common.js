/**
 * Created by Maryna on 7/26/2016.
 */


requirejs.config({
    appDir: ".",
   /* baseUrl: "http://localhost:63342/Traveling/dotnet/Traveling/Traveling.Portal/javascript/src/js",*/
    waitSeconds: 500,
    paths: {
        "jquery": ['https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min', 'libs/jquery-min'],
        "bootstrap": ['https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min', 'libs/bootstrap-min'],
        "main-add-my-story": ["/javascript/src/js/add-my-story/main-add-my-story"],
        "validation": ["./validation"],
        "send-info-to-server": ["./send-info-to-server"],
        "file-validation": ["./file-validation"],
        "send-photos": ["./send-photos"],
        "modal-windows": ["./modal-windows"],
        "left-list-margin": ["../left-list-margin"],
        "modal-error": ["./modal-error"],
        "filer": ['../../../jQuery.filer-1.0.5/js/jquery.filer'],
        "log-out": ['./log-out'],
        "main-moderation": ['/javascript/src/js/admin-moderation/main-moderation'],
        "delete-photo": ['/javascript/src/js/admin-moderation/delete-photo'],
        "text-moderation-and-approved": ['/javascript/src/js/admin-moderation/text-moderation-and-approved'],
        "decline-story": ['/javascript/src/js/admin-moderation/decline-story'],
        "modal-moderation": ['/javascript/src/js/admin-moderation/modal-moderation'],
        "stories-list": ['./stories-list'],
        "widget-3-stories": ['./widget-3-stories'],
        "domReady": ["../domReady"],
        "styled-empty-strings": ['../add-my-story/styled-empty-strings'],
        "request-story": ['./request-story'],
        "request-stories-list": ['../index/request-stories-list'],
        "full-story": ['../full-story/full-story'],
        "request-full-story": ['../full-story/request-full-story'],
        "carousel": ["../../../lightbox-master/lightbox-master/dist/ekko-lightbox"]
    },
    shim: {

        "left-list-margin": {
            deps: ["jquery"],
            exports: "left-list-margin"
        },

        "bootstrap": {
            deps: [
                'jquery'
            ]
        },
        "styled-empty-strings": {
            deps: ["bootstrap"],
            exports: "styledEmptyStrings"
        },
        "filer": {
            deps: ["styled-empty-strings"],
            exports: "filerObj"
        },
        "modal-error": {
            deps: ["bootstrap"],
            exports: "modalObj"
        },
        "stories-list": {
            deps: ["bootstrap"],
            exports: "storiesList"
        },
        "request-story": {
            deps: ["bootstrap"],
            exports: "styledEmptyStrings"
        },
        "main-moderation": {
            deps: ["jquery"],
            exports: "mainMod"
        },
        "full-story": {
            deps: ["jquery"],
            exports: "full"
        }
    }
});


