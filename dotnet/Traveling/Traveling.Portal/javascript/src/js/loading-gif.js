﻿define([], function () {

    var LoadingAnimation = {

        startLoadingAnimation: function () {

            var imgObj = $("#loadImg");
            imgObj.show();

            //var leftOffsetForm = $(".form-group").offset().left; 

            var centerY = $(window).scrollTop() + ($(window).height() + imgObj.height()) / 2;
            var centerX;

            if ($(".jFiler-input-inner").length) {
                centerX = $(".jFiler-input-inner").width() / 2 + $(".jFiler-input-inner").offset().left - $("#loadImg").width() / 2;
            }
            else {
                centerX = $("#navbarMenu").width() / 2 + $("#navbarMenu").offset().left - $("#loadImg").width() / 2;
            }            

            imgObj.offset({ "top": centerY, "left": centerX });
        },

        stopLoadingAnimation: function () {
            $("#loadImg").hide();
        }
    };

    return LoadingAnimation;
});