/**
 * Created by Maryna on 12/9/2016.
 */

define ([], function (){
    $('textarea').keyup(function(event){
        if($(event.target).val().length && $(event.target).hasClass( "redBorder" )) {
            $(event.target).removeClass( "redBorder" )
        }
    });
});