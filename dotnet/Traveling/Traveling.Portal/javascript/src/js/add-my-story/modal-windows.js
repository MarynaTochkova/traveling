/**
 * Created by Maryna on 10/27/2016.
 */

define ([], function () {
    var modalWindows = {

        centerModalWindow: function () {
            var centerX = $(".jFiler-input-inner").width() / 2 + $(".jFiler-input-inner").offset().left - $(".modal-dialog").width() / 2
        },
        noPhoto: function () {
            var modalObj = $("#modalNoPhoto").modal();
            modalObj.modal('show');
            var context = this;
            modalWindows.centerModalWindow();
            $("#closeModalNoPhoto").click(function () {
                modalObj.modal('hide');
                $("#sendButton").prop('disabled', false);
            });         
        },
        fillAllFields: function () {
            var modalObj = $("#modalAllFields").modal();
            modalObj.modal('show');
            var context = this;
            context.centerModalWindow();
            $("#closeModalAllFields").click(function () {
                modalObj.modal('hide');
                $("#sendButton").prop('disabled', false);
            });
        },
        
        wasSentToModerator: function () {
            var modalObj = $("#wasSentToModerator").modal();
            modalObj.modal('show');
            var context = this;
            context.centerModalWindow();
            $("#closeWasSentToModerator").click(function () {
                modalObj.modal('hide');
                window.location.assign("../../../index.html");
            });
        },

        errorGeneral: function () {
            var modalObj = $("#errorGeneral").modal();
            modalObj.modal('show');
            var context = this;
            context.centerModalWindow();
            $("#closeErrorGeneral").click(function () {
                modalObj.modal('hide');
                $("#sendButton").prop('disabled', false);
            });
        },

        errorServerPhoto: function () {
            var modalObj = $("#errorServerPhoto").modal();
            modalObj.modal('show');
            var context = this;
            context.centerModalWindow();
            $("#closeErrorServerPhoto").click(function () {
                modalObj.modal('hide');
                $("#sendButton").prop('disabled', false);
            });
        },

        fileIsBiggerOrEmpty: function () {

            var modalObj = $("#fileIsBiggerOrEmpty").modal();
            modalObj.modal('show');
            var context = this;
            context.centerModalWindow();
            $("#closeFileIsBiggerOrEmpty").click(function () {
                modalObj.modal('hide');
                $("#sendButton").prop('disabled', false);
            });
            $('li.jFiler-item').each(function () {
                if (!$(this).find('img').length) {
                    $(this).remove();
                }
            });
        },

        fileIsNotImg: function () {
            var modalObj = $("#fileIsNotImg").modal();
            modalObj.modal('show');
            var context = this;
            context.centerModalWindow();
            $("#closeFileIsNotImg").click(function () {
                modalObj.modal('hide');
                $("#sendButton").prop('disabled', false);
            });
            $('li.jFiler-item').each(function () {
                if (!$(this).find('img').length) {
                    $(this).remove();
                }
            });
        }

    };

    return modalWindows;
});