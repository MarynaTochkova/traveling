﻿define(["../common", "../loading-gif"], function (a, loadingObj) {
    var i, photoId;
    var photosRespond = {};
    photosRespond[i] = i;
    photosRespond[photoId] = photoId;

    loadingObj.startLoadingAnimation();

    var photoRespondObj = {

        ajaxPreparation: function (photo) {

            var urlAndformDataObj = {};

            //converting img to Blob

            function base64ToBlob(base64, mime) {
                mime = mime || '';
                var sliceSize = 1024;
                var byteChars = window.atob(base64);
                var byteArrays = [];
        
                for (var offset = 0, len = byteChars.length; offset < len; offset += sliceSize) {
                    var slice = byteChars.slice(offset, offset + sliceSize);

                    var byteNumbers = new Array(slice.length);
                    for (var i = 0; i < slice.length; i++) {
                        byteNumbers[i] = slice.charCodeAt(i);
                    }

                    var byteArray = new Uint8Array(byteNumbers);

                    byteArrays.push(byteArray);
                }

                return new Blob(byteArrays, { type: mime });
            }

            var formData = new FormData();
            var image = $(photo).context.querySelector('img').getAttribute('src');
            var base64ImageContent = image.replace(/^data:image\/(png|jpeg|jpg);base64,/, "");
            var blob = base64ToBlob(base64ImageContent, 'image/png');
            formData.append('picture', blob);

            var webSiteUrl = window.location.protocol + "//" + window.location.host + "/";
            if (location.pathname.split('/').length > 5) {
                webSiteUrl += location.pathname.split('/')[1];
            }

            urlAndformDataObj['url'] = webSiteUrl;
            urlAndformDataObj['formData'] = formData;

            return urlAndformDataObj; 
        },

        sendPhotoToServ: function (url, formData) {
            console.log("inside sendPhotoToServ");
            return $.ajax({
                url: url + "/api/picture",
                data: formData,
                type: 'POST',
                processData: false,  // tell jQuery not to process the data
                contentType: false,   // tell jQuery not to set contentType
                method: 'POST',
                cache: false,
                success: function (data, textStatus, xhr) {
                    console.log("inside ajax send photos request success");
                },
                error: function (xhr, textStatus, errorThrown) {
                    console.log(xhr.status + ': ' + xhr.statusText);
                    require(["modal-windows"], function (modalWindows) {
                        loadingObj.stopLoadingAnimation();
                        modalWindows.errorServerPhoto();
                    });
                }
            });
        }
    };

    return photoRespondObj;
});
