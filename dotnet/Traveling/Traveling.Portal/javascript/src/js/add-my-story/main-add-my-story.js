﻿


define(["../common"], function (){

            require (["filer"], function (filerObj){
                var fileList = [];
                var idArray = [];

                $(document).ready(function () {

                    //clearing text fields after refresh
                    (function () {
                        $($('textarea')[0]).val('');
                        $($('textarea')[1]).val('');
                        $($('textarea')[2]).val('');
                    })();

                        var webSiteUrl = window.location.protocol + "//" + window.location.host + "/";
                        if (location.pathname.split('/').length > 5) {
                            webSiteUrl += location.pathname.split('/')[1];
                        }                                                                 

                        $("#filer_input").filer({
                            limit: null,
                            maxSize: null,
                            extensions: null,
                            changeInput: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div><div class="jFiler-input-text"><h3>Drag&Drop files here</h3> <span style="display:inline-block; margin: 15px 0">or</span></div><a class="jFiler-input-choose-btn blue">Browse Files</a></div></div>',
                            showThumbs: true,
                            theme: "dragdropbox",
                            templates: {
                                box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
                                item: '<li class="jFiler-item">\
                                <div class="jFiler-item-container">\
                                    <div class="jFiler-item-inner">\
                                        <div class="jFiler-item-thumb">\
                                            <div class="jFiler-item-status"></div>\
                                            <div class="jFiler-item-info">\
                                                <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                                <span class="jFiler-item-others">{{fi-size2}}</span>\
                                            </div>\
                                            {{fi-image}}\
                                        </div>\
                                        <div class="jFiler-item-assets jFiler-row">\
                                            <ul class="list-inline pull-left">\
                                                <li>{{fi-progressBar}}</li>\
                                            </ul>\
                                            <ul class="list-inline pull-right">\
                                                <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                            </ul>\
                                        </div>\
                                    </div>\
                                </div>\
                            </li>',
                                itemAppend: '<li class="jFiler-item">\
                                    <div class="jFiler-item-container">\
                                        <div class="jFiler-item-inner">\
                                            <div class="jFiler-item-thumb">\
                                                <div class="jFiler-item-status"></div>\
                                                <div class="jFiler-item-info">\
                                                    <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                                    <span class="jFiler-item-others">{{fi-size2}}</span>\
                                                </div>\
                                                {{fi-image}}\
                                            </div>\
                                            <div class="jFiler-item-assets jFiler-row">\
                                                <ul class="list-inline pull-left">\
                                                    <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                                </ul>\
                                                <ul class="list-inline pull-right">\
                                                    <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                                </ul>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </li>',
                                progressBar: '<div class="bar"></div>',
                                itemAppendToEnd: false,
                                removeConfirmation: true,
                                _selectors: {
                                    list: '.jFiler-items-list',
                                    item: '.jFiler-item',
                                    progressBar: '.bar',
                                    remove: '.jFiler-item-trash-action'
                                }
                            },
                            dragDrop: {
                                dragEnter: null,
                                dragLeave: null,
                                drop: null,
                            },
                            
                            files: null,
                            addMore: true,
                            clipBoardPaste: true,
                            excludeName: null,
                            beforeRender: null,
                            afterRender: null,
                            beforeShow: null,
                            beforeSelect: null,
                            onSelect: null,
                            afterShow: null,
                            onEmpty: null,
                            options: null,
                            captions: {
                                button: "Choose Files",
                                feedback: "Choose files To Upload",
                                feedback2: "files were chosen",
                                drop: "Drop file here to Upload",
                                removeConfirmation: "Are you sure you want to remove this file?",
                                errors: {
                                    filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
                                    filesType: "Only Images are allowed to be uploaded.",
                                    filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-maxSize}} MB.",
                                    filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB."
                                }
                            }
                    });

                    require(["file-validation"], function (photoValidObject) {
                        $('#form').on('change', function (event) {
                            photoValidObject.onChangeEvent(event);
                        });
                    });

                    //delete photo  

                    $(document).on('click', '.icon-jfi-trash.jFiler-item-trash-action', function () {
                        $(event.target).closest("li.jFiler-item").remove();
                    });

                    /*send story to moderator */

                    $("button#sendButton").on("click", function (event) {
                        console.log("sendButton clicked");
                        $("#sendButton").prop('disabled', true);
                        //arrayOfPhotos
                        $('li.jFiler-item').each(function () {
                            fileList.push(this);
                        });

                            event.preventDefault();
                            idArray = [];
                            if ($('li.jFiler-item').length) {
                                require(["validation", "../loading-gif"], function (validObj, loadingObj) {
                                    loadingObj.startLoadingAnimation();
                                    var validatRes = validObj.validObj();
                                    if (validatRes) {
                                        var quantitySuccessPhotos = 0;
                                        require(["send-photos", "send-info-to-server"], function (servPhoto, servObj) {

                                            var itemsProcessed = 0;

                                            $.each.call(this, fileList, function (index, value) {

                                                $.when(sendPhoto(value)).done(function (r) {
                                                    console.log("inside sendPhoto(value)).done");
                                                    if (r) {
                                                        var photoId = r.Pictures[0].Id;
                                                        idArray.push(photoId);

                                                        async function asyncFunc() {
                                                            itemsProcessed++;
                                                            if (itemsProcessed === fileList.length) {
                                                                console.log('All photos were send. Sending the story text');
                                                                sendTextAndShowResult();
                                                            }
                                                        };
                                                        asyncFunc();
                                                    }

                                                    else {
                                                        require(["modal-windows"], function (modalWindows) {
                                                            loadingObj.stopLoadingAnimation();
                                                            modalWindows.errorServerPhoto();
                                                        });
                                                    }
                                                });

                                                function sendPhoto(a) {
                                                    console.log("inside sendPhoto");
                                                    var preparingData = servPhoto.ajaxPreparation(a);
                                                    var webSiteUrl = preparingData.url;
                                                    var formData = preparingData.formData;

                                                    return servPhoto.sendPhotoToServ(webSiteUrl, formData);

                                                };

                                            });
                                            //sending the text object to the server if sending photos is ok
                                            function sendTextAndShowResult() {

                                                fileList = [];
                                                validatRes.Pictures = idArray;

                                                (function sendTextObject(object) {
                                                    return servObj.sendToServ(JSON.stringify(object));
                                                })(validatRes);
                                            };
                                        });
                                    }
                                });
                            }
                            else {
                                require (["modal-windows" ], function (modalWindows){
                                    modalWindows.noPhoto();
                                });
                            }
                        });
                    });
            });
});
