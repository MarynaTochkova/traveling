/**
 * Created by Maryna on 10/20/2016.
 */

define(["../common", "../loading-gif"], function (c, loadingObj) {

    var validMethods = {
        cityName: function () {
            return $( "textarea#cityName" ).val();
        },
        storyHeadline: function () {
            return $( "textarea#storyHeadline" ).val();
        },
        story: function () {
            return $( "textarea#story" ).val();
        },
        validObj: function () {
            if(this.cityName() && this.story() && this.storyHeadline()) {
                return {
                    city: this.cityName(),
                    title: this.storyHeadline(),
                    text: this.story()
                }
            }
            else {
                if (!this.cityName()) {
                    $( "textarea#cityName" ).addClass('redBorder');
                }
                if (!this.storyHeadline()) {
                    $( "textarea#storyHeadline" ).addClass('redBorder');
                }
                if (!this.story()) {
                    $( "textarea#story" ).addClass('redBorder');
                }

                require (["modal-windows" ], function (modalWindows){
                    modalWindows.fillAllFields();
                    loadingObj.stopLoadingAnimation();
                });
                return false;
            }
        }
    };
    
    return validMethods; 
   });