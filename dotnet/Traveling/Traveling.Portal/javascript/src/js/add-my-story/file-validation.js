﻿
define(["../common", "modal-windows"], function (c, modal) {

    //var fileList = []; 

    var photoValidObject = {

        onChangeEvent: function (event) {

            photoValidObject.file = 0;
            photoValidObject.filesArray = [];
            var typeOk = true;
            var sizeOk = true;

            if (!event.target.files[0] /*&& event.target.files.length == 1*/) {
                sizeOk = false;
                modal.fileIsBiggerOrEmpty();
            }

            $(event.target.files).each(function (index) {

                //var currentFile = $('.jFiler-item-container')[index]; 
                //var currentLi = currentFile.closest('li');
                if (event.target.files[index]) {

                    var fileType = event.target.files[index].type;
                    var validImageTypes = ["image/gif", "image/jpeg", "image/png"];

                    if ($.inArray(fileType, validImageTypes) < 0) {
                        modal.fileIsNotImg();
                        typeOk = false;
                    }
                    //checking a size of file
                    else if (typeOk) {

                        var byteSize = event.target.files[index].size;

                        function bytesToSize(bytes) {
                            const size = (bytes / 1024 / 1024).toFixed(2);
                            return size;
                        };

                        var MbSize = bytesToSize(byteSize);

                        if (MbSize >= 5 || MbSize == 0) {
                            //currentLi.style.display = "none";
                            modal.fileIsBiggerOrEmpty();
                            sizeOk = false;
                            return;
                        }
                        //else {

                        //    if (event.target.files.length == 1) {
                        //        photoValidObject.file = event.currentTarget.getElementsByTagName('li')[index];
                        //    }

                        //    else if (event.target.files.length > 1) {
                        //        //this.file.push(event.currentTarget.getElementsByTagName('li')[index]);

                        //        if (index == 0) {
                        //            photoValidObject.filesArray.push(event.currentTarget.getElementsByTagName('li')[index]);
                        //        }
                        //        else if (index > 0) {
                        //            photoValidObject.filesArray.push(event.currentTarget.getElementsByTagName('li')[index + 2]);
                        //        }

                        //    }

                        //}
                    }

                    else {
                        sizeOk = false;
                        modal.fileIsBiggerOrEmpty();
                    }
                }
            })
        },

        //file: 0,
        //filesArray: []
    };

    return photoValidObject; 
           
});