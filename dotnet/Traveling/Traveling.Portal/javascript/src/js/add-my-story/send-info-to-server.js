/**
 * Created by Maryna on 10/20/2016.
 */

define(["../common", "../loading-gif"], function (a, loadingObj) {

    return {

              sendToServ: function (obj) {

              var webSiteUrl = window.location.protocol + "//" + window.location.host + "/";
              if (location.pathname.split('/').length > 5) {
                  webSiteUrl += location.pathname.split('/')[1];
              }

              return $.ajax({  
                  type: 'post',
                  url: webSiteUrl + 'api/story',
                  data: obj,
                  contentType: "application/json; charset=utf-8",
                  traditional: true,
                  success: function(data, textStatus, xhr) {
                          console.log('Text was sent, success');
                          require(["modal-windows"], function (modalWindows) {
                                loadingObj.stopLoadingAnimation();
                                modalWindows.wasSentToModerator();
                           });
                  },
                  error: function (xhr, textStatus, errorThrown) {
                      console.log("Error by sending story to server" + " " + errorThrown.status);
                      require(["modal-windows"], function (modalWindows) {
                           loadingObj.stopLoadingAnimation();
                           modalWindows.errorGeneral();
                      });
                  }
              })   
          },
    };
});
