﻿
define([], function () {
    return function (arrayOf3First) {


        for (var i = 0; i < 3; i++) {
            (function (i) {
                console.log(i);
                    ajaxCall(arrayOf3First[i], i);
            })(i);
        }; 


        function ajaxCall(value, index) {
            console.log(value, index);
            var cityId = $(value).attr("data-id");

            var webSiteUrl = window.location.protocol + "//" + window.location.host + "/";

            if (location.pathname.split('/').length > 5) {
                webSiteUrl += location.pathname.split('/')[1];
            };

            var callUrl = webSiteUrl + 'api/widget/' + cityId;

            $.ajax({
                type: 'get',
                url: callUrl,
                contentType: "application/json; charset=utf-8",
                traditional: true,
                success: function (data, textStatus, xhr) {

                    var city = data,
                        name = city.CityName,
                        title = city.StoryTitle,
                        firstPicture = city.ShownPicture,
                        photoId = city.ShownPicture.Id,

                        cityId = city.CityId,
                        image = new Image();

                    $('a.3-stories-click')[index].innerText = name + ": " + title;
                    
                    image.src = firstPicture.PictureData;

                    var newPhoto = `<img src=${image.src}  alt=${photoId} data-id=${cityId} class="imageStyleHoriz img-responsive">`;
                    $($("a.thumbnail")[index]).append(newPhoto);
                    $($("a.3-stories-click")[index]).attr('href', 'javascript/src/html/full-story-templ.html' + '?cityId=' + cityId);
                },
                error: function (xhr, textStatus, errorThrown) {
                    console.log("Error in getting stories from server" + " " + errorThrown.status);
                }
            });
        };         
    };
});