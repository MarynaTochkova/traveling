 /**
 * Created by Maryna on 6/27/2016.
 */

require(["javascript/src/js/common.js"], function (a){

    require(["left-list-margin"], function () {

        //check if Browser is EI
        var version = detectIE();

        if (version === true) {
            alert("I am much more beautiful in Chrome or Firefox");
        }; 


        function detectIE() {
            var ua = window.navigator.userAgent;

            var msie = ua.indexOf('MSIE ');
            if (msie > 0) {
                // IE 10 or older => return version number
                return true;
            }

            var trident = ua.indexOf('Trident/');
            if (trident > 0) {
                // IE 11 => return version number
                var rv = ua.indexOf('rv:');
                return true;
            }

            var edge = ua.indexOf('Edge/');
            if (edge > 0) {
                // Edge (IE 12+) => return version number
                return true;
            }

            // other browser
            return false;
        }

        //go to admin authentication
        (function ($) {
            $("#admin").on("click", function () {
                window.location.href = 'javascript/src/html/admin-authentication.html';
            });
        })(jQuery);

        require(["request-stories-list"], function (a) {

        });
    });
 });

