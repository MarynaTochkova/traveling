﻿define(["jquery", 'domReady!'], function ($) {

    var webSiteUrl = window.location.protocol + "//" + window.location.host + "/";
    if (location.pathname.split('/').length > 5) {
        webSiteUrl += location.pathname.split('/')[1];
    }
    var callUrl = webSiteUrl + 'api/city';

    $.ajax({
        type: 'get',
        url: callUrl,
        contentType: "application/json; charset=utf-8",
        traditional: true,
        success: function (data, textStatus, xhr) {

            var response = data;

            $.each(response, function (index, value) {

                var cityId = value.CityId;
                const listIt
                    = `
                <a href=${'javascript/src/html/full-story-templ.html' + '?cityId=' + cityId} class="listStyle" data-id=${cityId}>${value.Name} <span class="badge">${value.Stories.length}</span></a> <br>
                    `;
                $(".list-group").append(listIt);
            });

            require(["widget-3-stories"], function (widgetsRequest) {

                var arrayOf3First = [],
                    lengthAList = $(".list-group").find("a").length,
                    last = $(".list-group").find("a")[lengthAList - 1],
                    lastMinus1 = $(".list-group").find("a")[lengthAList - 2],
                    lastMinus2 = $(".list-group").find("a")[lengthAList - 3];

                arrayOf3First.push(last);
                arrayOf3First.push(lastMinus1);
                arrayOf3First.push(lastMinus2);
                widgetsRequest(arrayOf3First); 
            });
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log("Error in getting stories from server" + " " + errorThrown.status);
        }
    });
});