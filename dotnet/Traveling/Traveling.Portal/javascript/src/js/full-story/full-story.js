﻿
define(["../common"], function (a) {

    require(['jquery'], function ($) {

        $(".flex-container").hide();

        require(['../loading-gif'], function (loadingObj) {

            loadingObj.startLoadingAnimation();
            //getting id from query
            const urlParams = new URLSearchParams(window.location.search);
            const cityId = urlParams.get('cityId');

                require(['request-full-story'], function (funct) {
                    funct(cityId, loadingObj);
                });
            });
        });
    //});
});

