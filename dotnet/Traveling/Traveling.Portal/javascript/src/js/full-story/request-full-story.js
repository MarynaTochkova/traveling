﻿define(["bootstrap"], function () {

            return function (cityId, loadingObj) {

                var cityId = cityId; 
                var webSiteUrl = window.location.protocol + "//" + window.location.host + "/";
                if (location.pathname.split('/').length > 5) {
                    webSiteUrl += location.pathname.split('/')[1];
                };
                var callUrl = webSiteUrl + 'api/city/' + cityId;

                $.ajax({
                    type: 'get',
                    url: callUrl,
                    contentType: "application/json; charset=utf-8",
                    traditional: true,
                    success: function (data, textStatus, xhr) {


                        require(['carousel'], function () {

                            var city = data;
                            var photos = [];
                            var basicStoryDom = $(".story").clone("true");

                            var basicAddFunc = function (value) {
                                $(".title").last().text(city.Name);
                                $(".shortIntro").last().text(value.Title);
                                $(".fullText").last().text(value.Text);
                                $(".flex-container").show();

                                $.each(value.Pictures, function (index, photoValue) {
                                    var photoId = photoValue.Id;
                                    var image = new Image();
                                    image.src = photoValue.PictureData;

                                    //add new photo to carousel                                   

                                    var newPhoto =
                                        `<a href=${image.src} data-toggle="lightbox" data-gallery="example-gallery" class="col-sm-4" data-max-width="600">
                                            <img src=${image.src} alt=${photoId} class="img-fluid">
                                         </a>`;
                                    photos.push(newPhoto);
                                    //last item
                                    if (index == value.Pictures.length - 1) {
                                        var lastRow = `<div class="row"> ${photos.join("")} </div>`;
                                        $(".col-md-8").last().append(lastRow);
                                        $(".col-md-8").last().on('click', '[data-toggle="lightbox"]', function (event) {
                                            event.preventDefault();
                                            $(this).ekkoLightbox();
                                        }); 
                                    }
                                    //last item in a row
                                    else if ((index + 1) % 3 == 0) {
                                        var rowOfThree = `<div class="row"> ${photos.join("")} </div>`;
                                        $(".col-md-8").last().append(rowOfThree);
                                        photos = [];                                        
                                    }                                    
                                });
                            };

                            $.each(city.Stories, function (index, value) {
                                if (index == 0) {
                                    basicAddFunc(value);
                                }
                                else if (index > 0) {
                                    $(".flex-container").append(basicStoryDom.clone(true));
                                    $(".story").last().attr('id', ("story-" + (index + 1)));
                                    photos = []; 
                                    basicAddFunc(value);
                                }

                                loadingObj.stopLoadingAnimation();
                                //$(".flex-container").show();
                            });

                        });
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        console.log("Error in getting stories from server" + " " + errorThrown.status);
                    }
                });
            };
        });


