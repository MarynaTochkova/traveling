/**
 * Created by Maryna on 11/15/2016.
 */

define(["../common" ], function (com){

    require (["modal-error"], function (modalObj){

        $(".btn-block").click(function () {
            var objToServer = {
                user: $( "input[placeholder='Login']" ).val(),
                password: $( "input[placeholder='Password']" ).val()
            };
            
            var webSiteUrl = window.location.protocol + "//" + window.location.host + "/";
            if (location.pathname.split('/').length > 5) {
                webSiteUrl += location.pathname.split('/')[1];
            }
            var callUrl = webSiteUrl + 'api/login';
            $.ajax({
                type: 'post',
                url: callUrl,
                data: JSON.stringify(objToServer),
                contentType: "application/json; charset=utf-8",
                traditional: true,
                success: function(data, textStatus, xhr) {
                    var cookie = data;
                    window.location.assign("admin-moderation.html");
                },
                error: function (xhr, textStatus, errorThrown) {
                    modalObj();
                }
            });
        });
    });

});
