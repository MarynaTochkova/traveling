/**
 * Created by Maryna on 11/15/2016.
 */

define(["require","../common",  "bootstrap"], function (require){
    
    var loginError = function () {
        var modalObj = $("#modalErrorLoginPass").modal();
        modalObj.modal('show');
        $("#closeErrorLoginPass").click(function () {
            modalObj.modal('hide');
        });
    };
    return loginError;
});


