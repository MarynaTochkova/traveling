﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using Hangfire;
using Hangfire.SQLite;
using Microsoft.Owin;
using Owin;
using Travelling.Entity;
using Travelling.Infrastructure;
using Travelling.Portal.Middleware;
using GlobalConfiguration = Hangfire.GlobalConfiguration;

[assembly: OwinStartup(typeof(Travelling.Portal.Startup))]

namespace Travelling.Portal
{
    public class Startup
    {

        private static readonly string ServerUploadFolder = "~/App_Data/Temp";


        public void Configuration(IAppBuilder app)
        {
            var options = new SQLiteStorageOptions();

            //create temp folder if it does not exist
            var tempFolderPath = HostingEnvironment.MapPath(ServerUploadFolder);
            if (!Directory.Exists(tempFolderPath))
            {
                Directory.CreateDirectory(tempFolderPath);
            }

            GlobalConfiguration.Configuration.UseSQLiteStorage("SQLiteConnection", options);

            var option = new BackgroundJobServerOptions { WorkerCount = 1 };
            app.UseHangfireServer(option);

            app.UseHangfireDashboard();

            RecurringJob.AddOrUpdate( 
                () => CleanupPictures(),
                Cron.Hourly);

            var config = new HttpConfiguration();

            config.Services.Replace(typeof(IExceptionHandler), new CustomExceptionHandler());

            app.UseWebApi(config);
            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888
        }


        public async Task CleanupPictures()
        {
            Debug.WriteLine($"Picture cleanup task at {DateTime.Now}");

            var serviceLocator = System.Web.Http.GlobalConfiguration.Configuration.DependencyResolver;
            var db = (ITravellingDb)serviceLocator.GetService(typeof(ITravellingDb));
            var blobService = (IBlobStorageService)serviceLocator.GetService(typeof(IBlobStorageService));
            var pictures = db.Query<Picture>().Where(p =>
                p.StoryId == Guid.Empty && p.UploadedTime != null);

            foreach (var pic in pictures)
            {
                if (DateTime.UtcNow.Subtract(pic.UploadedTime) < TimeSpan.FromHours(1))
                {
                    continue;
                }
                await blobService.DeleteBlobAsync(pic.Name);
                db.Remove(pic);
            }

            await db.SaveChangesAsync();
        }
    }
}
