﻿
Write-Host "Starting project deploy"

Set-ExecutionPolicy  RemoteSigned

Import-AzurePublishSettingsFile  .\test-app11.PublishSettings

Publish-AzureWebsiteProject -ProjectFile .\Traveling.Portal.csproj -Name test-app11 -Configuration Debug -ConnectionString @{ DefaultConnection = "my connection string" }

##includes
#. ..\_library\ENV.ps1
#. ..\_library\IIS.ps1
#. ..\_library\DB.ps1


#    function Stop-JMS{ 

#        $jms = (Get-Process -Name "MIP.MarketBackend.JobManagerService" -ErrorAction SilentlyContinue)    
#        if($jms){
#            Stop-Process -Name "MIP.MarketBackend.JobManagerService"
#        }

#    }
	
#	function script:exec
#	{
#    	[CmdletBinding()]
#		param(
#		[Parameter(Position=0,Mandatory=1)][scriptblock]$cmd,
#		[Parameter(Position=1,Mandatory=0)][string]$errorMessage = ("Error executing command: {0}" -f $cmd)
#		)
#		& $cmd
#		#if ($lastexitcode -ne 0)
#		#{
#		#	throw $errorMessage
#		#}
#	}



##do not allow script to continue ef error occured
#$global:ErrorActionPreference = "Stop"

#Stop-JMS

#write-host "For details how to run the script please refer to the README.TXT document" -foregroundcolor "green"	

## START JMS
#$jms_application_path = "c:\temp\jms\MIP.MarketBackend.JobManagerService.exe"


#Start-Process $jms_application_path "-console"


#Stop-JMS



