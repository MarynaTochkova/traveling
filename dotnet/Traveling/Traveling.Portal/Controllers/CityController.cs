﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Travelling.Entity;
using Travelling.Infrastructure;
using Travelling.Portal.Infrastructure;
using Travelling.Portal.Models;

namespace Travelling.Portal.Controllers
{
    [GzipCompressed]
    public class CityController : ApiController
    {
        // GET: City
        private readonly ITravellingDb _db;
        private readonly IBlobStorageService _blobService;

        public CityController(IBlobStorageService blobStorageService, ITravellingDb db)
        {
            Validator.IsNotNull(blobStorageService, () => blobStorageService);
            Validator.IsNotNull(db, () => db);

            _blobService = blobStorageService;
            _db = db;
        }

        [System.Web.Http.Route("api/city")]
        [System.Web.Http.HttpGet]
        public async Task<IEnumerable<GroupedCityViewModel>> GetControl()
        {
            //paging?
            //profile db?
            var rows = await (from st in _db.Query<Story>()
                join ct in _db.Query<City>() on st.CityId equals ct.Id
                where st.IsApproved
                select new { ct.Name, st.Id, st.CityId }).ToListAsync();

            var stories = from row in rows
                group row by row.Name into g
                select new GroupedCityViewModel { Name = g.Key.ToString(), CityId = g.FirstOrDefault().CityId, Stories = g.ToList().Select(s => s.Id) };


            return stories;

        }

        [System.Web.Http.Route("api/city/{cityId}")]
        [System.Web.Http.HttpGet]
        [OutputCache(Duration = 360000, VaryByParam = "cityId")]
        public async Task<CityViewModel> Get(int cityId)
        {

            var stories = await (from s in _db.Query<Story>().Include("Pictures").Include("City")
                                where s.CityId == cityId && s.IsApproved
                select s).ToListAsync();

           
            if (stories == null || !stories.Any())
            {
                throw new HttpException("Not Found");
            }

            var model = new CityViewModel
            {
                Id = stories.FirstOrDefault().City.Id,
                Name = stories.FirstOrDefault().City.Name,
                Stories = new List<StoryModelView>()
            };

            foreach (var story in stories)
            {
                model.Stories.Add(new StoryModelView
                {
                    Id = story.Id,
                    Text = story.Text,
                    Title = story.Title,
                    Pictures = story.Pictures.Select(pic => new PictureViewModel()
                    {
                        Id = pic.Id,
                        OriginalName = pic.OriginalName,
                        Name = pic.Name,
                        PictureData = "data:image/png;base64," + Convert.ToBase64String(System.IO.File.ReadAllBytes(_blobService.GetFilePath(pic.Name)))
                    }).ToList()
                });
            }

            return model;
        }

        [System.Web.Http.Route("api/widget/{cityId}")]
        [System.Web.Http.HttpGet]
        [OutputCache(Duration = 360000, VaryByParam = "cityId")]
        public async Task<WidgetItemViewModel> GetWidget(int cityId)
        {

            var story = await (from s in _db.Query<Story>().Include("Pictures").Include("City")
                where s.CityId == cityId && s.IsApproved
                select s).FirstOrDefaultAsync();

            if (story == null)
            {
                throw new HttpException("Story Not Found");
            }

            var model = new WidgetItemViewModel
            {

                CityId =  story.City.Id,
                CityName = story.City.Name,
                StoryId = story.Id,
                StoryText = story.Text,
                StoryTitle = story.Title
            };

            var pic = story.Pictures.FirstOrDefault();

            if (pic == null)
            {
                throw new HttpException("Picture Not Found");
            }

            model.ShownPicture = new PictureViewModel
            {
                Id = pic.Id,
                OriginalName = pic.OriginalName,
                Name = pic.Name,
                PictureData = "data:image/png;base64," +
                              Convert.ToBase64String(System.IO.File.ReadAllBytes(_blobService.GetFilePath(pic.Name)))
            };

            return model;
        }
    }
}
