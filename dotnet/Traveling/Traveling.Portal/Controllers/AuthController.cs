﻿using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Security;
using Travelling.Portal.Models;

namespace Travelling.Portal.Controllers
{
    public class AuthController : ApiController
    {
        [System.Web.Http.Route("api/login")]
        [HttpPost]
        [AllowAnonymous]
        public HttpResponseMessage Login(LoginViewModel model)
        {
            var userName = WebConfigurationManager.AppSettings["adminName"];
            var password = WebConfigurationManager.AppSettings["adminPass"];

            if (model.UserName == userName && model.Password == password)
            {
                FormsAuthentication.SetAuthCookie(model.UserName, true);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized);
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpPost, AllowAnonymous, Route("api/logout")]
        public void Signout()
        {
            FormsAuthentication.SignOut();

            if (HttpContext.Current.Session != null)
                HttpContext.Current.Session.Abandon();
        }
    }
}
