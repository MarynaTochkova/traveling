﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using Travelling.Entity;
using Travelling.Infrastructure;
using Travelling.Portal.Infrastructure;
using Travelling.Portal.Models;

namespace Travelling.Portal.Controllers
{
    public class StoryController : ApiController
    {
        private readonly ITravellingDb _db;

        private readonly ICacheProviderService _cache;
        private readonly IBlobStorageService _blobService;

        public StoryController(ICacheProviderService cacheProviderService, IBlobStorageService blobStorageService, ITravellingDb db)
        {
            Validator.IsNotNull(cacheProviderService, ()=> cacheProviderService);
            Validator.IsNotNull(blobStorageService, ()=> blobStorageService);
            Validator.IsNotNull(db, ()=> db);

            _cache = cacheProviderService;
            _blobService = blobStorageService;
            _db = db;
        }

        [Route("api/story")]
        [HttpPost]
        public async Task<HttpResponseMessage> Post(NewStoryViewModel newStory)
        {
            newStory.Id = Guid.NewGuid();

            var city = await (from c in _db.Query<City>()
                where c.Name == newStory.City
                select c).FirstOrDefaultAsync();

            if (city == null)
            {
                city = new City {Name = newStory.City};
                city.Id =_db.Add(city).Id;
            }

            var story = new Story
            {
                Id = newStory.Id,
                CityId = city.Id,
                IsApproved = false,
                Text = newStory.Text,
                Title = newStory.Title
            };

            var dbStory = _db.Add(story);

            if (newStory.Pictures != null)
            {
                var pictures = _db.Query<Picture>().Where(p => newStory.Pictures.Contains(p.Id.ToString()));

                foreach (var picture in pictures)
                {
                    picture.StoryId = dbStory.Id;
                }
            }

            await _db.SaveChangesAsync();
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        [Route("api/story")]
        [HttpGet]
        public async Task<IEnumerable<StoryModelView>> Get()
        {
           //paging?
            //profile db?
            var stories = await (from s in _db.Query<Story>()
                where s.IsApproved
                select s).ToListAsync();

            return stories.Select(item => new StoryModelView
            {
                City = item.City.Name, Id = item.Id, Text = item.Text, Title = item.Title, Pictures = item.Pictures.Select(pic => new PictureViewModel()
                {
                    Id = pic.Id, OriginalName = pic.OriginalName, Name = pic.Name
                }).ToList()
            });
        }


        [Route("api/story")]
        [HttpPut]
        public async Task<HttpResponseMessage> Update(NewStoryViewModel model)
        {
            if (model.Id == Guid.Empty)
            {
                throw new HttpException("Id is missing");
            }

            var story = await _db.Query<Story>().FirstOrDefaultAsync(s => s.Id == model.Id);

            if (story == null)
            {
                throw new HttpException("Story was not found");
            }

            City city = null;
            try
            {
                city = await (from c in _db.Query<City>()
                    where c.Name == model.City
                    select c).FirstOrDefaultAsync();
            }
            catch (Exception exp)
            {

            }

            

            story.Text = model.Text;
            story.Title = model.Title;

            if (city == null)
            {
                city = new City { Name = model.City };
                city.Id = _db.Add(city).Id;
            }

            story.City = city;


            _db.Update(story);

            await _db.SaveChangesAsync();
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

       

        [Route("api/story/{id}")]
        [HttpGet]
        public async Task<StoryModelView> Get(Guid id)
        {
            //paging?
            var story = await (from s in _db.Query<Story>()
                                 where s.Id == id
                                 select s).FirstOrDefaultAsync();
            var story1 = await (from s in _db.Query<Story>().Include("Pictures")
                               select s).ToListAsync();
            story = story1.FirstOrDefault(s => s.Id == id);
            if (story == null)
            {
                throw new HttpException("Not Found");
            }
            if (!story.IsApproved && !User.Identity.IsAuthenticated)
            {
                throw new HttpException("User must be authenticated to access this story");
            }

            return new StoryModelView
            {
                City = story.City.Name,
                Id = story.Id,
                Text = story.Text,
                Title = story.Title,
                Pictures = story.Pictures.Select(pic => new PictureViewModel()
                {
                    Id = pic.Id,
                    OriginalName = pic.OriginalName,
                    Name = pic.Name
                }).ToList()
            };
        }

        [Route("api/story/moderate")]
        [HttpGet]
        [Authorize]
        public async Task<IEnumerable<StoryModelView>> GetModerated()
        {
            //paging?
            var stories = await (from s in _db.Query<Story>()
                                 where !s.IsApproved 
                                 select s).ToListAsync();

            return stories.Select(item => new StoryModelView
            {
                City = item.City.Name,
                Id = item.Id,
                Text = item.Text,
                Title = item.Title,
                Pictures = item.Pictures.Select(pic => new PictureViewModel()
                {
                    Id = pic.Id,
                    OriginalName = pic.OriginalName,
                    Name = pic.Name
                }).ToList()
            });
        }

	    [Route("api/test/{id}")]
	    [HttpGet]
	    [Authorize]
        [GzipCompressed]
        public async Task<StoryModelView> GetTest(Guid id)
	    {
			//paging?
			var story = await (from s in _db.Query<Story>()
							   where s.Id == id
							   select s).FirstOrDefaultAsync();
			var story1 = await (from s in _db.Query<Story>().Include("Pictures")
								select s).ToListAsync();
			story = story1.FirstOrDefault(s => s.Id == id);

			if (story == null)
		    {
			    throw new HttpException("Not Found");
		    }
		    if (!story.IsApproved && !User.Identity.IsAuthenticated)
		    {
			    throw new HttpException("User must be authenticated to access this story");
		    }

		    return new StoryModelView
		    {
			    City = story.City.Name,
			    Id = story.Id,
			    Text = story.Text,
			    Title = story.Title,
			    Pictures = story.Pictures.Select(pic => new PictureViewModel()
			    {
				    Id = pic.Id,
				    OriginalName = pic.OriginalName,
				    Name = pic.Name,
				    PictureData = "data:image/png;base64," + Convert.ToBase64String(System.IO.File.ReadAllBytes(_blobService.GetFilePath(pic.Name)))
			    }).ToList()
		    };
	    }

		[Route("api/story/approve/{id}")]
        [HttpPut]
        [Authorize]
        public async Task<HttpResponseMessage> Approve(Guid id, Story story)
        {
            var dbStory = await (from s in _db.Query<Story>()
                where s.Id == id
                select s).FirstAsync();
                dbStory.IsApproved = true;
                dbStory.Text = story.Text;
                dbStory.Title = story.Title;
            await _db.SaveChangesAsync();
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Route("api/story/{id}")]
        [HttpDelete]
        [Authorize]
        public async Task<HttpResponseMessage> Delete(Guid id)
        {
            var pictures = await (from p in _db.Query<Picture>()
                where p.StoryId == id
                select p).ToArrayAsync();

            foreach (var picture in pictures)
            {
                await _blobService.DeleteBlobAsync(picture.Name);
            }

            _db.RemoveRange(pictures);

            var story = await(from s in _db.Query<Story>()
                where s.Id == id
                select s).FirstAsync();

            _db.Remove(story);
            await _db.SaveChangesAsync();
            return Request.CreateResponse(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}
