﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json;
using Travelling.Entity;
using Travelling.Infrastructure;
using Travelling.Portal.Infrastructure;
using Travelling.Portal.Models;

namespace Travelling.Portal.Controllers
{
    public class PictureController : ApiController
    {
        private static readonly string ServerUploadFolder = "~/App_Data/Temp"; //Path.GetTempPath();

        private readonly ICacheProviderService _cache;
        private readonly IBlobStorageService _blobService;
        private readonly ITravellingDb _db;


       public PictureController(ICacheProviderService cacheService, IBlobStorageService blobStorageService, ITravellingDb db)
        {
            Validator.IsNotNull(cacheService, ()=> cacheService);
            Validator.IsNotNull(blobStorageService, ()=> blobStorageService);
            Validator.IsNotNull(db, () => db);

            _cache = cacheService;
            _blobService = blobStorageService;
            _db = db;
        } 
       

        [Route("api/picture")]
        [HttpPost]
        [ValidateMimeMultipartContentFilter]
        public async Task<StoryPicturesViewModel> UploadSingleFile()
        {
            string root = HttpContext.Current.Server.MapPath(ServerUploadFolder);
            var streamProvider = new MultipartFormDataStreamProvider(root);

            try
            {
                await Request.Content.ReadAsMultipartAsync(streamProvider);

                if (!streamProvider.FileData.Any())
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest){Content = new StringContent("No photo was found")});
                }
                var vm = new StoryPicturesViewModel { Pictures = new List<UploadedPictureViewModel>()};
                var tempPictures = new List<Picture>();
                var i = 0;
                foreach (var image in streamProvider.FileData)
                {
                    var originalName = image.Headers.ContentDisposition.FileName.Trim('"');
                    var filePath = image.LocalFileName;
                    var fileName = string.Concat(filePath.Split('\\').Last(), ".", originalName.Split('.').Last());
                    
                    await _blobService.AddBlob(fileName, filePath);

                    var picture = new Picture
                    {
                        OriginalName = originalName,
                        Name = fileName,
                        UploadedTime = DateTime.UtcNow
                    };

                   var dbResp =  _db.Add(picture);
                   tempPictures.Add(dbResp);
                   File.Delete(filePath);
                   
                    vm.Pictures.Add(new UploadedPictureViewModel { OriginalName = originalName, Name = fileName, Id = i });
                    i++;
                }
                await _db.SaveChangesAsync();
                foreach (var picture in vm.Pictures)
                {
                    picture.Id = tempPictures[picture.Id].Id;
                }
                return vm;
            }
            catch (Exception e)
            {
                throw;
                //return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [GzipCompressed]
        [Route("api/picture")]
        [HttpGet]
        public async Task<HttpResponseMessage> DownloadImage(string name)
        {

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StreamContent(new FileStream(_blobService.GetFilePath(name), FileMode.Open, FileAccess.Read));
            response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");

            var imageMetadata =  await (from p in _db.Query<Picture>()  where p.Name == name select p).FirstOrDefaultAsync();
            response.Content.Headers.ContentDisposition.FileName = imageMetadata.OriginalName;
            response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(_blobService.GetMimeType(imageMetadata.OriginalName));

            return response;
        }


        [HttpDelete]
        [Route("api/picture/{pictureId}/{storyId}")]
        [Authorize]
        public async Task<HttpResponseMessage> DeletePicture(int pictureId, Guid storyId)
        {
            //remove picture
            var picture = await _db.Query<Picture>().Where(s => s.Id == pictureId).FirstOrDefaultAsync();

            if (picture == null)
            {
                throw new Exception("The picture was not found");
            }

            if (!_blobService.BlobExists(picture.Name))
            {
               return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
            await _blobService.DeleteBlobAsync(picture.Name);



            //update story
            var story = await _db.Query<Story>().Where(s => s.Id == storyId).FirstOrDefaultAsync();
            if (story != null)
            {
                var itemToRemove = story.Pictures.Single(r => r.Id == pictureId);
                story.Pictures.Remove(itemToRemove);
                _db.Update(story);
            }

            _db.Remove(picture);

            //try
            //{
            //    var pictures = JsonConvert.DeserializeObject<List<Picture>>(await _cache.Get(storyId.ToString()));
            //    var newPictures = pictures.Where(p => p.Id != picture.Id).ToList();
            //    //todo: remember why I used cache
            //    //await _cache.Add(storyId.ToString(), JsonConvert.SerializeObject(newPictures));
            //}
            //catch (Exception exp)
            //{
            //    //todo: Add logging
            //}

            await _db.SaveChangesAsync();

            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }


        [HttpPut]
        [Route("api/picture/{pictureId}")]
        [Authorize]
        public async Task<HttpResponseMessage> UpdatePicture(int pictureId)
        {

            var root = HttpContext.Current.Server.MapPath(ServerUploadFolder);
            var streamProvider = new MultipartFormDataStreamProvider(root);

            try
            {
                await Request.Content.ReadAsMultipartAsync(streamProvider);

                if (!streamProvider.FileData.Any())
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent("No photo was found") });
                }

                if (streamProvider.FileData.Count() > 1)
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent("Only one file should be sent") });
                }

                var image = streamProvider.FileData.FirstOrDefault();
                var tempPictures = new List<Picture>();
                var filePath = image.LocalFileName;

                var picture = _db.Query<Picture>().FirstOrDefault(p => p.Id == pictureId);

                if (picture == null)
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError) { Content = new StringContent("Old picture was not found") });
                }

                await _blobService.UpdateBlob(_blobService.GetFilePath(picture.Name), filePath);

                File.Delete(filePath);

            }
            catch (Exception e)
            {
                throw;
                //return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }


            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }

        public static Bitmap ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.Default;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.Default;
                graphics.PixelOffsetMode = PixelOffsetMode.Default;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }
    }
}
