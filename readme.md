# Traveling
It is a non-commercial project that allows people to share their travel experiences, photos and unforgettable moments. The user can submit a new story enjoying user-friendly design. The moderator can then either publish the story or make the required changes. After the story is published, it can be seen on the website. All stories are grouped by cities which helps users to quickly navigate around. 
One can feel the atmosphere of a city even more deeply with the help of convenient Photocarousel.
The project makes the story creation and viewing fun.

## Version 
* 1.0

## Browser and Screen Support
* Firefox;
* Chrome;
* No mobile responsive support yet. 

## Frontend Part of the Project (was developed by Tochkova Maryna)
* [link to the js, css, html folders](https://bitbucket.org/MarynaTochkova/traveling/src/master/dotnet/Traveling/Traveling.Portal/javascript/src/);
* [link to index.html folder](https://bitbucket.org/MarynaTochkova/traveling/src/master/dotnet/Traveling/Traveling.Portal/).


## Used technoligies
* Fronted: JavaScript, ES6,  jQuery, Ajax, Bootstrap, CSS, HTML, RequireJS;
* Backend: .NET 4.6.2, ASP.NET API (OWIN/KATANA), SQLLite, Hangfire.

## Software development environment 
* Sourcetree, Trello, Git, Postman, Visual Studio,	Fiddler.

## Methodology 
* Kanban

## Link to the project in Internet
This is [a Website of the Project](http://ec2-18-184-226-172.eu-central-1.compute.amazonaws.com/index.html).
Please, use Firefox or Chrome.

## Credentials to the moderator page
* Login: admin
* Password: Pass123

## Demo
Welcome [to the demo of the project on YouTube](https://www.youtube.com/watch?v=wMk677cQPCw).
